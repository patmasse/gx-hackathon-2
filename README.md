# Overview of Hackathon #2

We are delighted to invite the Gaia-X community to the second official Gaia-X Hackathon. **The Hackathon will take place on the 2nd and 3rd of December 2021.**

The Gaia-X Hackathon is a two days event with each day being crowned with a presentation session of showcases that leverage components and concepts of the Gaia-X architecture. You can register for the Hackathon and the showcase sessions separately.

The goals of the Gaia-X Hackathon are

1. to increase the technical competence and knowledge related to technology relevant to the Gaia-X ecosystem within the community and
1. to integrate and align different ideas, concepts, pilots and prototypes to consistent approaches.

The Gaia-X Hackathon will be organized as a community event in regular intervals by the Open Work Package Minimal Viable Gaia-X/Piloting.

# Getting in Touch

For discussions related to the Hackathon you can use the mailing list of the MVG/Piloting Open Work Package. You can subscribe to it via the following link:
https://list.gaia-x.eu/postorius/lists/wp-minimal-viable-gaia.list.gaia-x.eu/

Additionally, we have established a Matrix room for general discussions on the Hackathon which you may want to join (if you do not have any user client so far, please install one of the
following: https://matrix.org/clients/) - the created room is: gaia-x-hackathon

# Further Information

You'll find the schedule and wiki right [here](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-2/-/wikis/GX-Hackathon-2).

